# C++ BLAS/LAPACK Implementation

adblaslapack is BLAS/LAPACK implementation with operator overloading support for
automatic differentiation in C++. The code is based on the f2c generated BLAS implementation (http://www.netlib.org/clapack/).
